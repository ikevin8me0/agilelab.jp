function validate_company_name(company_name, validation) {
    if (!company_name.value) {
        company_name.classList.add("is-invalid");
        validation = false;
    } else {
        company_name.classList.remove("is-invalid");
    }
    return validation;
}

function validate_contact_person(contact_person, validation) {
    if (!contact_person.value) {
        contact_person.classList.add("is-invalid");
        validation = false;
    } else {
        contact_person.classList.remove("is-invalid");
    }
    return validation;
}

function validate_email_address(email_address, validation) {
    if (!email_address.value.includes("@") || !email_address.value.includes(".")) {
        email_address.classList.add("is-invalid");
        validation = false;
    } else {
        email_address.classList.remove("is-invalid");
    }
    return validation;
}

function set_submission_in_progress() {
    var submit_button = document.getElementById("submit_button");
    submit_button.style.display = 'none';
    var submission_in_progress = document.getElementById('submission_in_progress');
    submission_in_progress.style.display = 'block';
}

function show_submit_button() {
    var submit_button = document.getElementById("submit_button");
    submit_button.style.display = 'block';
    var submission_in_progress = document.getElementById('submission_in_progress');
    submission_in_progress.style.display = 'none';
}

function hide_inquiry_form() {
    var inquiry_form = document.getElementById("inquiry_form");
    inquiry_form.style.display = 'none';
}

function show_inquiry_form() {
    var inquiry_form = document.getElementById("inquiry_form");
    inquiry_form.style.display = 'block';
}

function show_form_submission_success() {
    var form_post_response = document.getElementById("form_post_response");
    form_post_response.style.display = 'block';
}

function hide_form_post_error() {
    var form_post_response = document.getElementById("form_post_error");
    form_post_response.style.display = 'none';
}

function show_form_post_error() {
    var form_post_response = document.getElementById("form_post_error");
    form_post_response.style.display = 'block';
}

function submitform()
{
    var submitted = false;
    var validation = true;
    var company_name = document.forms["inquiry_form"]["company_name"];
    var contact_person = document.forms["inquiry_form"]["contact_person"];
    var email_address = document.forms["inquiry_form"]["email_address"];
    var phone_number = document.forms["inquiry_form"]["phone_number"];
    var inquiry_message = document.forms["inquiry_form"]["inquiry_message"];
    validation = validate_company_name(company_name, validation);
    validation = validate_contact_person(contact_person, validation);
    validation = validate_email_address(email_address, validation);
    if(validation) {
        var http = new XMLHttpRequest();
        http.open("POST", "https://apps.capitalten.co.jp/hello_server/agilelab/inquiry", true);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        var params = "company_name=" + company_name.value +
            "&contact_person=" + contact_person.value +
            "&email_address=" + email_address.value +
            "&phone_number=" + phone_number.value +
            "&inquiry_message=" + inquiry_message.value;
        http.send(params);
        http.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                hide_form_post_error();
                hide_inquiry_form();
                show_form_submission_success();
                submitted = true;
            }
        }
        http.onerror = function() {
            if (!submitted) {
                show_form_post_error();
                show_submit_button();
            }
        }
        set_submission_in_progress();
    }
}
